# siccegge.zsh-theme

A `oh-my-zsh` theme based on the theme of a dear colleague.

![](screen2.png)

![](screen1.png)

# Features

- widely spaced prompt with separation by newline
- full working directory is shown (except for `$HOME -> ~` substitution)
- current working directory on separate line (the cursor will always start at the same
  position, regardless of how deep the hierarchy)
- exit status of command directly visible (colored green or red)
- current time (minute resolution)
- git prompt
  - general status
  - warn if you're not on the develop branch
  - show number of branches ahead or behind the branch
    - master or main *if* on a dev branch in `ZSH_THEME_GIT_DEVELOPMENT_BRANCHES`
    - dev branch *if* on master or main
    - dev branch *if* on any other branch
- battery prompt

# zsh configuration recommendations

- `setopt transient_rprompt` to only show the right prompt on the current command line

# Installation

Copy the theme to the oh-my-zsh custom dir:

    cp siccegge.zsh-theme "$ZSH_CUSTOM"/themes/

Edit `.zshrc` to use the theme

    ZSH_THEME="siccegge"

# Configuration

You may set a few variables:

Show the oh-my-zsh git prompt

    ZSH_THEME_GIT_SHOW=1

Show a red `!` before the git prompt if you're not on any of the following branches:

    ZSH_THEME_GIT_WARN_ABOUT_BRANCH_STRING=!
    ZSH_THEME_GIT_DEVELOPMENT_BRANCHES=(dev devel develop)

Show the number of commits your branch is ahead or behind on either master (when on the
develop branch) or develop branch (when on master branch or != master or develop):

    ZSH_THEME_GIT_SHOW_AHEAD_BEHIND=1


Show battery capacity (requires the `battery` plugin loaded):

    plugins=(
    	...
    	battery
    )
    ZSH_THEME_BATTERY=1

We have support for the `(h)ledger` timeclock plaintext accounting/time tracking solution. Set
the following to see when you're clocked in:

    ZSH_THEME_CLOCKEDIN=1
    TIMECLOCKS=(~/company1.journal ~/company2.journal)
