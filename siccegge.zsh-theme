function clockedin () {
	CLOCKED_IN=()
	for timeclock in $TIMECLOCKS; do
		if [ -f "$timeclock" ]; then
			ll=$(tac "$timeclock" | egrep -m 1 '^(o|i) ')
			if [[ "$ll" == "i"* ]]; then
				CLOCKED_IN+=$(awk '{print $4}'<<<$ll)
			fi
		fi
	done
	if (( ${#CLOCKED_IN[@]} > 0 )); then
		printf "Clocking: %s" "${CLOCKED_IN[*]}"
	fi
}

function ahead_behind() {
	local ahead behind comparebranch ref masterbranch devbranch
	masterbranch=master
	devbranch=master

	ref=$(__git_prompt_git symbolic-ref --short HEAD 2> /dev/null) \
	|| ref=$(__git_prompt_git rev-parse --short HEAD 2> /dev/null) \
	|| return 0

	ZSH_MASTER_BRANCHES=(master main)
	for branch in $ZSH_MASTER_BRANCHES; do
		for gitbranch in $(__git_prompt_git branch -a); do
			if [[ $gitbranch == $branch ]]; then
				masterbranch=$branch
				break
			fi
		done
	done
	for branch in $ZSH_THEME_GIT_DEVELOPMENT_BRANCHES; do
		for gitbranch in $(__git_prompt_git branch -a); do
			if [[ $gitbranch == $branch ]]; then
				devbranch=$branch
				break
			fi
		done
	done
	if [[ $ref != $masterbranch ]] && [[ $ref != $devbranch ]]; then
		comparebranch=$devbranch
	elif [[ $ref == $devbranch ]]; then
		comparebranch=$masterbranch
	elif [[ $ref == $masterbranch ]]; then
		comparebranch=$devbranch
	fi
	# echo $masterbranch $devbranch $comparebranch $ref
	ahead=$(__git_prompt_git rev-list ${comparebranch}..HEAD 2>/dev/null | wc -l)
	behind=$(__git_prompt_git rev-list HEAD..${comparebranch} 2>/dev/null | wc -l)

	(( ahead == 0 )) || echo -n "${ZSH_THEME_GIT_PROMPT_AHEAD}${ahead} "
	(( behind == 0 )) || echo -n "${ZSH_THEME_GIT_PROMPT_BEHIND}${behind} "
}

function warn_about_development_branch() {
	if ! __git_prompt_git rev-parse --git-dir &> /dev/null \
		|| [[ "$(__git_prompt_git config --get oh-my-zsh.hide-info 2>/dev/null)" == 1 ]]; then
		return 0
	fi

	local ref
	ref=$(__git_prompt_git symbolic-ref --short HEAD 2> /dev/null) \
	|| ref=$(__git_prompt_git rev-parse --short HEAD 2> /dev/null) \
	|| return 0

	if (( ${+ZSH_THEME_GIT_DEVELOPMENT_BRANCHES} )); then
		for warnbranch in $ZSH_THEME_GIT_DEVELOPMENT_BRANCHES; do
			for gitbranch in $(__git_prompt_git branch -a); do
				if [[ $gitbranch == $warnbranch ]] && [[ $warnbranch != $ref ]]; then
					echo "${ZSH_THEME_GIT_WARN_ABOUT_BRANCH_STRING:-(!)}"
					return
				fi
			done
		done
	fi
}

PROMPT=$'\n%{$fg[white]%}%n@%{$fg[magenta]%}%m%{$reset_color%} %~\n%{$fg[cyan]%}%T%{$reset_color%} %0(?,%{$fg[green]%},%{$fg_bold[red]%}%s)%?%{$reset_color%} %# %{$reset_color%}'
PROMPT2='➜ '

RPROMPT=''
if (( ${+ZSH_THEME_GIT_AHEAD_BEHIND} )); then
	RPROMPT="$RPROMPT"$'$(ahead_behind)'
fi
if (( ${+ZSH_THEME_GIT_DEVELOPMENT_BRANCHES} )); then
	RPROMPT="$RPROMPT"$'%{$fg_bold[red]%}$(warn_about_development_branch)%{$reset_color%} '
fi
if (( ${+ZSH_THEME_GIT_SHOW} )); then
	RPROMPT="$RPROMPT"$'$(git_prompt_info) $(git_prompt_status)%{$reset_color%}'
fi
if (( ${+ZSH_THEME_BATTERY} )); then
	RPROMPT="$RPROMPT"$'$(battery_pct_prompt)%{$reset_color%}'
fi
if (( ${+ZSH_THEME_CLOCKEDIN} )); then
	RPROMPT="$RPROMPT"$'$(clockedin)%{$reset_color%}'
fi

ZSH_THEME_GIT_WARN_ABOUT_BRANCH_STRING="!"
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}git [%{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[cyan]%}]"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[cyan]%}] %{$fg_bold[green]%}✔%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg_bold[blue]%}✚ "
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg_bold[yellow]%}● "
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg_bold[red]%}✖ "
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg_bold[blue]%}➜ "
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg_bold[cyan]%}§ "
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg_bold[white]%}… "
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg_bold[green]%}⚑ "
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg_bold[white]%}↑ "
ZSH_THEME_GIT_PROMPT_BEHIND="%{$fg_bold[white]%}↓ "
